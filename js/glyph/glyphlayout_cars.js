function glyphlayout() {
    var margin = {top: 0, right: 0, bottom: 10, left: 20},
        width = 600 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    /*
     * value accessor - returns the value to encode for a given data object.
     * scale - maps value to a visual display encoding, such as a pixel position.
     * map function - maps from data value to display value
     * axis - sets up axis
     */

    // Set glyph properties
    var glyphSize = height / 28,
        glyphRmax = glyphSize / 2,
        glyphRmin = glyphRmax / 3;

    var rScale = d3.scale.linear().range([glyphRmin, glyphRmax]);

    // setup x
    var xValue = function (d) {
            return d['mdsX'];
        }, // data -> value
        xScale = d3.scale.linear().range([glyphRmax, width - glyphRmax]), // value -> display
        xMap = function (d) {
            return xScale(xValue(d));
        }, // data -> display
        xAxis = d3.svg.axis().scale(xScale).orient('bottom');

    // setup y
    var yValue = function (d) {
            return d['mdsY'];
        }, // data -> value
        yScale = d3.scale.linear().range([height - glyphRmax, glyphRmax]), // value -> display
        yMap = function (d) {
            return yScale(yValue(d));
        }, // data -> display
        yAxis = d3.svg.axis().scale(yScale).orient('left');

    // setup fill color
    var cValue = function (d) {
            return d.cylinders;
        },
        color = d3.scale.category10();

    // Set zoom behaviour
    var zoom = d3.behavior.zoom().x(xScale).y(yScale).scaleExtent([1, 10]).on('zoom', zoomed);

    // Add the main svg element
    var svg = d3.select('.glyphlayout')
                .on('touchstart', nozoom)
                .on('touchmove', nozoom)
                .append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // Add the zoomable area
    var container = svg.append('g').call(zoom);
    // Draw a rect to see the the interactive area
    var rect = container.append('rect')
                        .attr('width', width)
                        .attr('height', height)
                        .style('fill', 'none')
                        .on('click', clicked)
                        .style('pointer-events', 'all')
                        .style('stroke-width', 1)
                        .style('stroke', 'black');

    var view = container.append('g').attr('class', 'view');

    // Create a force layour to place the data
    // var force = d3.layout.force()
    //     .size([width, height]);

    // add the tooltip area to the webpage
    var tooltip = d3.select('.glyphlayout').append('div').attr('class', 'tooltip').style('opacity', 0);

    // load data
    d3.csv('data/cars.csv', function (error, cars) {
        if (error) throw error;

        // change string (from CSV) into number format

        /*
         * get the numeric traits of the data set and calculate basic metrics
         */
        var meanByDim = {},
            medianByDim = {},
            domainByDim = {},
            domByDim = {};

        var dimensions = d3.keys(cars[0]).filter(function (dim) {
            return dim !== 'name'
                && ( meanByDim[dim] = d3.mean(cars, function (d) {
                    d[dim] = +d[dim];
                    return d[dim];
                }) )
                && ( medianByDim[dim] = d3.median(cars, function (d) {
                    return d[dim];
                }) )
                && ( domainByDim[dim] = d3.extent(cars, function (d) {
                    d[dim] = ( (d[dim] == '') ? medianByDim[dim] : +d[dim]);
                    return d[dim];
                }) );
        });
        var n = dimensions.length;
        var m = cars.length;

        // Find symetrical domain around mean
        dimensions.forEach(function (dim) {
            var rangeAbove = domainByDim[dim][1] - meanByDim[dim];
            var rangeBelow = meanByDim[dim] - domainByDim[dim][0];
            domByDim[dim] = (rangeAbove > rangeBelow) ?
                [meanByDim[dim] - rangeAbove, meanByDim[dim] + rangeAbove] :
                [meanByDim[dim] - rangeBelow, meanByDim[dim] + rangeBelow];
        });

        var D = cars.map(function (d1, i) {
            var dist = [];
            cars.forEach(function (d2, j) {
                dist.push(distance(d1, d2, dimensions));
            });
            return dist;
        });

        var flowerPositions = mds(D);

        cars.forEach(function (d, i) {
            d.id = i;
            d.mdsX = flowerPositions[i][0];
            d.mdsY = flowerPositions[i][1];
            // dimensions.forEach( function (dim) {
            //   (d[dim] == '')? medianByDim[dim] : +d[dim];
            // })
        });

        // Construct nodes and links
        // var nodes = flowers.map( function (d) {
        //     return {x: xValue(d), y: yValue(d)};
        // })
        // var links = [];
        // flowers.forEach( function (d1, i) {
        //     flowers.forEach( function (d2, j) {
        //         if (i < j) {
        //             var link = {source: i, target: j, value: distance(d1, d2)}
        //             links.push(link);
        //         }
        //     })
        // })

        // force.nodes(nodes).links(links);
        // force.linkDistance(50)
        //     .charge(-100)
        //     .on("tick", tick);

        // don't want glyphs overlapping axis, so add in buffer to data domain
        xScale.domain(d3.extent(cars, xValue));
        yScale.domain(d3.extent(cars, yValue));

        // x-axis
        svg.append('g')
           .attr('class', 'x axis')
           .attr('transform', 'translate(0,' + height + ')')
           .call(xAxis).append('text').attr('class', 'label')
           .attr('x', width).attr('y', -6)
           .style('text-anchor', 'end')
           .text('MDS1');

        // y-axis
        svg.append('g')
           .attr('class', 'y axis')
           .call(yAxis)
           .append('text')
           .attr('class', 'label')
           .attr('transform', 'rotate(-90)')
           .attr('y', 6)
           .attr('dy', '.71em')
           .style('text-anchor', 'end')
           .text('MDS2');

        // set glyphs
        var glyphs = view.selectAll('.poneglyph')
                         .data(cars)
                         .enter()
                         .append('g')
                         .attr('class', 'poneglyph')
                         .attr('transform', function (d) { return 'translate(' + xMap(d) + ',' + yMap(d) + ')'; });

        // draw glyphs
        glyphs.selectAll('circle')
              .data([glyphRmin, glyphRmax])
              .enter()
              .append('circle')
              .attr('class', 'guide')
              .attr('r', function (d) { return d; });

        glyphs.append('circle')
              .attr('r', glyphRmin)
              .style('fill', function (d) { return color(cValue(d)); });

        var stepAngle = 2 * Math.PI / n;
        dimensions.forEach(function (dim, i) {
            rScale.domain(domByDim[dim]);
            var arc = d3.svg.arc()
                        .startAngle(i * stepAngle)
                        .endAngle((i + 1) * stepAngle)
                        .innerRadius(function (d) {
                            return (d[dim] > meanByDim[dim]) ? rScale(meanByDim[dim]) : rScale(d[dim]);
                        })
                        .outerRadius(function (d) {
                            return (d[dim] > meanByDim[dim]) ? rScale(d[dim]) : rScale(meanByDim[dim]);
                        });

            glyphs.append('path')
                  .attr('class', 'arc')
                  .attr('d', arc)
                  .style('fill', function (d) {
                      return (d[dim] > meanByDim[dim]) ? 'darkgreen' : 'red';
                  })
                  .style('opacity', 0.7);
        });

        // set tooltip
        glyphs.on('mouseover', function (d) {
            tooltip.transition().duration(200).style('opacity', .9);
            tooltip.html(tooltipHtmlText(d)).style('left', (d3.event.pageX + 5) + 'px').style('top', (d3.event.pageY - 28) + 'px');
        }).on('mouseout', function (d) {
            tooltip.transition().duration(500).style('opacity', 0);
        });

        function tooltipHtmlText(d) {
            var text = d.name + ' (' + d['id'] + ')<br/>';
            dimensions.forEach(function (dim) {
                text = text + dim + ": " + d[dim] + '<br/>';
            });
            return text;
        }

        // draw legend
        var legend = svg.selectAll('.legend').data(color.domain()).enter().append('g').attr('class', 'legend').attr('transform',
            function (d, i) {
                return 'translate(0,' + i * 20 + ')';
            });

        // draw legend colored rectangles
        legend.append('rect').attr('x', width - 18).attr('width', 18).attr('height', 18).style('fill', color);

        // draw legend text
        legend.append('text').attr('x', width - 24).attr('y', 9).attr('dy', '.35em').style('text-anchor', 'end').text(function (d) {
            return d;
        });

        // computeguides(d) {}
        // computearcs(d) {}
        // function poneglyph(d){}
    });

    function zoomed() {
        svg.select('.x.axis').call(xAxis);
        svg.select('.y.axis').call(yAxis);
        view.attr('transform',
            'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
    }

    function clicked(d, i) {
        if (d3.event.defaultPrevented) return; // zoomed

        d3.select(this).transition().style('fill', 'black').transition().style('fill', 'white');
    }

    function nozoom() {
        d3.event.preventDefault();
    }

    function distance(a, b, dimensions) {
        var sum = 0;
        dimensions.forEach(function (dim) {
            sum += Math.pow(a[dim] - b[dim], 2);
        });
        var d = Math.pow(sum, 1 / dimensions.length);
        return d;
    }

    function mds(distances, dimensions) {
        dimensions = dimensions || 2;

        // square distances
        var M = numeric.mul(-0.5, numeric.pow(distances, 2));

        // double centre the rows/columns
        function mean(A) {
            return numeric.div(numeric.add.apply(null, A), A.length);
        }

        var rowMeans = mean(M),
            colMeans = mean(numeric.transpose(M)),
            totalMean = mean(rowMeans);

        for (var i = 0; i < M.length; ++i) {
            for (var j = 0; j < M[0].length; ++j) {
                M[i][j] += totalMean - rowMeans[i] - colMeans[j];
            }
        }

        // take the SVD of the double centred matrix, and return the
        // points from it
        var ret = numeric.svd(M),
            eigenValues = numeric.sqrt(ret.S);
        return ret.U.map(function (row) {
            return numeric.mul(row, eigenValues).splice(0, dimensions);
        });
    };

    // function identity(n) {
    //     var m = new Array(n);
    //     var i, j;
    //     for (i=0; i<n; i++) {
    //         m[i] = new Array(n);
    //         for (j=0; j<n; j++) {
    //             if (i === j) {
    //                 m[i][j] = 1;
    //             }
    //             else {
    //                 m[i][j] = 0;
    //             }
    //         }
    //     }
    //     return m;
    // }
    //
    // function sub(a, b) {
    //     var aNumRows = a.length, aNumCols = a[0].length,
    //         bNumRows = b.length, bNumCols = b[0].length,
    //         m = new Array(aNumRows);  // initialize array of rows
    //     for (var r = 0; r < aNumRows; ++r) {
    //         m[r] = new Array(bNumCols); // initialize the current row
    //         for (var c = 0; c < bNumCols; ++c) {
    //             m[r][c] = a[r][c] - b[r][c];
    //         }
    //     }
    //     return m;
    // }
    //
    // function multiply(a, b) {
    //     var aNumRows = a.length, aNumCols = a[0].length,
    //         bNumRows = b.length, bNumCols = b[0].length,
    //         m = new Array(aNumRows);  // initialize array of rows
    //     for (var r = 0; r < aNumRows; ++r) {
    //         m[r] = new Array(bNumCols); // initialize the current row
    //         for (var c = 0; c < bNumCols; ++c) {
    //             m[r][c] = 0;             // initialize the current cell
    //             for (var i = 0; i < aNumCols; ++i) {
    //                 m[r][c] += a[r][i] * b[i][c];
    //             }
    //         }
    //     }
    //     return m;
    // }
    //
    // function mult(num, a) {
    //     var aNumRows = a.length, aNumCols = a[0].length,
    //         m = new Array(aNumRows);  // initialize array of rows
    //     for (var r = 0; r < aNumRows; ++r) {
    //         m[r] = new Array(aNumCols); // initialize the current row
    //         for (var c = 0; c < aNumCols; ++c) {
    //             m[r][c] = num * a[r][c];
    //         }
    //     }
    //     return m;
    // }
}