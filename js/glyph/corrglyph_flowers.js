/**
 * Created by Jason on 3/27/2017.
 */
function corrGlyph() {
  var margin = {top: 30, right: 10, bottom: 10, left: 10},
      width = 380 - margin.left - margin.right,
      height = 380 - margin.top - margin.bottom;

  var svg = d3.select('.corrglyph').
      append('svg').
      attr('width', width + margin.left + margin.right).
      attr('height', height + margin.top + margin.bottom).
      append('g').
      attr('transform', 'translate(' + (width / 2 + margin.left) + ',' +
          (height / 2 + margin.top) + ')');

  // Set glyph properties
  var glyphRmax = Math.min(width, height) / 2;
  var glyphRmin = glyphRmax / 3;
  // var glyphRmean = 2 * glyphRmin

  svg.selectAll('circle').
      data([glyphRmin, glyphRmax]).
      enter().
      append('circle').
      attr('class', 'guide').
      attr('r', function(d) {
        return d;
      });

  var thScale = d3.scale.ordinal().rangePoints([0, 360], 1),
      rScale = {}, // scale for each dim radius
      tScale = {}, // scale for each dim tension
      dragging = {};

  var line = d3.svg.line.radial().interpolate('cardinal-closed');

  var axis = d3.svg.axis().orient('left'),
      background,
      foreground;

  // setup fill color
  var cValue = function(d) { return d.species; },
      color = d3.scale.category10();

  d3.csv('data/flowers.csv', function(error, flowers) {
    if (error) throw error;

    // Extract the list of dimensions and create a scale for each.
    thScale.domain(dimensions = d3.keys(flowers[0]).filter(function(dim) {
      return dim != 'species'
          && (rScale[dim] = d3.scale.linear().
              domain(d3.extent(flowers, function(p) {
                return +p[dim];
              })).
              range([glyphRmax - glyphRmin, 0]))
          && (tScale[dim] = d3.scale.linear().
          domain(d3.extent(flowers, function(p) {
              return +p[dim];
          })).range([1, 0]));
    }));

    // var n = dimensions.length;

    // Add grey background lines for context.
    background = svg.append('g').
        attr('class', 'background').
        selectAll('path').
        data(flowers).
        enter().
        append('path').
        attr('d', path);

    // Add blue foreground lines for focus.
    foreground = svg.append('g').
        attr('class', 'foreground').
        selectAll('path').
        data(flowers).
        enter().
        append('path').
        attr('d', path).
        style('stroke', function(d) { return color(cValue(d)); });

    // Add a group element for each dimension.
    var g = svg.selectAll('.dimension').
        data(dimensions).
        enter().
        append('g').
        attr('class', 'dimension').
        attr('transform', function(d) {
          return 'rotate(' + thScale(d) + ') ' + 'translate(0,' + -glyphRmax +
              ')';
        }).
        call(d3.behavior.drag().
            on('dragstart', dragstart).
            on('drag', dragmove).
            on('dragend', dragend)
        );

    // Add an axis and title.
    g.append('g').
        attr('class', 'axis').
        each(function(d) {
          d3.select(this).call(axis.scale(rScale[d]));
        }).
        append('text').
        style('text-anchor', 'middle').
        attr('y', -9).
        text(function(d) {
          return d;
        });

    // Add and store a brush for each axis.
    g.append('g').attr('class', 'brush').each(function(d) {
      d3.select(this).
          call(rScale[d].brush = d3.svg.brush().
              y(rScale[d]).
              on('brushstart', brushstart).
              on('brush', brush));
    }).selectAll('rect').attr('x', -8).attr('width', 16);

    function dragstart(dim) {
      dragging[dim] = thScale(dim);
      background.attr('visibility', 'hidden');
    }

    function dragmove(dim, i) {
      // console.log("Cartesian Coords: [" +d3.event.x + ", " + d3.event.y + "]");
      var coords = toPolar(d3.event.x, d3.event.y);
      // console.log("Polar Coords: [" + coords[0] + ", " + coords[1] + "]");
      dragging[dim] = coords[1];
      foreground.attr('d', path);
      dimensions.sort(function(a, b) {
        return theta(a) - theta(b);
      });
      thScale.domain(dimensions);
      g.attr('transform', function(d) {
        return 'rotate(' + theta(d) + ')' + 'translate(0,' + -glyphRmax + ')';
      });
    }

    function dragend(dim) {
      delete dragging[dim];
      transition(d3.select(this)).attr('transform',
          'rotate(' + thScale(dim) + ')' + 'translate(0,' + -glyphRmax +
          ')');
      transition(foreground).attr('d', path);
      background.attr('d', path).
          transition().
          delay(500).
          duration(0).
          attr('visibility', null);
    }
  });

  function theta(dim) {
    var v = dragging[dim];
    return v == null ? thScale(dim) : v;
  }

  function transition(g) {
    return g.transition().duration(500);
  }

  // Returns the path for a given data point.
  function path(d) {
      var normmean = 0;
      dimensions.forEach(function (dim) {
          normmean += tScale[dim](d[dim]);
      })
      normmean /= dimensions.length;
    return line.tension(normmean)(dimensions.map(function(dim) {
      return [(glyphRmax - rScale[dim](d[dim])), toRadians(theta(dim))];
      // return [ ( (glyphRmax - r[dim](d[dim])) * Math.sin(toRadians(theta(dim)))),
      //            (glyphRmax - r[dim](d[dim]))  * -1 * Math.cos(toRadians(theta(dim)))];
    }));
  }

  function brushstart() {
    d3.event.sourceEvent.stopPropagation();
  }

  // Handles a brush event, toggling the display of foreground lines.
  function brush() {
    var actives = dimensions.filter(function(p) {
          return !rScale[p].brush.empty();
        }),
        extents = actives.map(function(p) {
          return rScale[p].brush.extent();
        });
    foreground.style('display', function(d) {
      return actives.every(function(p, i) {
        return extents[i][0] <= d[p] && d[p] <= extents[i][1];
      }) ? null : 'none';
    });
  }

  // Aux functions
  function toDegrees(angle) {
    return angle * (180 / Math.PI);
  }

  function toRadians(angle) {
    return angle * (Math.PI / 180);
  }

  function toCartesian(r, t) {
    return [r * Math.cos(toRadians(t)), r * Math.sin(toRadians(t))];
  }

  function toPolar(x, y) {
    return [
      Math.sqrt(x * x + y * y),
      normalizeAngle(toDegrees(Math.atan2(x, -y)))];
  }

  function normalizeAngle(angle) {
    var newangle = angle;
    while (newangle <= 0) {
      newangle += 360;
    }
    while (newangle > 360) {
      newangle -= 360;
    }
    return newangle;
  }
}
